package com.example.sharif.foodorderapp.RecyclerViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sharif.foodorderapp.InterFace.ItemClickListener;
import com.example.sharif.foodorderapp.R;

/**
 * Created by SHARIF on 09-Sep-17.
 */
public class MenuViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView categoryNameTV;
    public ImageView categoryImageView;

    ItemClickListener itemClickListener;

    public MenuViewHolder(View itemView) {
        super(itemView);
        categoryImageView = (ImageView) itemView.findViewById(R.id.categoryImageId);
        categoryNameTV = (TextView) itemView.findViewById(R.id.categoryNameId);

        categoryImageView.setOnClickListener(this);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view,getAdapterPosition(),false);
    }
}
