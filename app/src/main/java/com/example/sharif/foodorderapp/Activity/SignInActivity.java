package com.example.sharif.foodorderapp.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sharif.foodorderapp.Model.SavedValue;
import com.example.sharif.foodorderapp.Model.User;
import com.example.sharif.foodorderapp.R;
import com.firebase.client.Firebase;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class SignInActivity extends AppCompatActivity {


    EditText phoneNumberET;
    EditText passwordET;
    Button signInBTN;

    String phoneNumber;
    String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        phoneNumberET = (EditText) findViewById(R.id.phoneNumberId);
        passwordET = (EditText) findViewById(R.id.passwordId);
        signInBTN = (Button) findViewById(R.id.signInId);

        signInBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                phoneNumber = phoneNumberET.getText().toString().trim();
                password = passwordET.getText().toString().trim();


                if(!TextUtils.isEmpty(phoneNumber) || !TextUtils.isEmpty(password)){

                    //Init frebase Database
                    FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
                    DatabaseReference table_user = firebaseDatabase.getReference("user");

                    final ProgressDialog progressDialog = new ProgressDialog(SignInActivity.this);
                    progressDialog.setMessage("Please Waiting......");
                    progressDialog.show();

                    table_user.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            //Check if user not exist in database.....
                            if(dataSnapshot.child(phoneNumber).exists()){
                                //Get user information......
                                progressDialog.dismiss();
                                User user = dataSnapshot.child(phoneNumber).getValue(User.class);
                                if(user.getPassword().equals(password)){
                                    Toast.makeText(SignInActivity.this, "Sign In Successfully", Toast.LENGTH_SHORT).show();
                                    SavedValue.currentUser = user;
                                    Intent homeIntent = new Intent(SignInActivity.this,HomeActivity.class);
                                    startActivity(homeIntent);
                                    finish();

                                }else{
                                    Toast.makeText(SignInActivity.this, "Password does not matched", Toast.LENGTH_SHORT).show();

                                }
                            }else {
                                Toast.makeText(SignInActivity.this, "User not exists in database", Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                }
                else{
                    Toast.makeText(SignInActivity.this, "Fields does not be empty", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
