package com.example.sharif.foodorderapp.InterFace;

import android.view.View;

/**
 * Created by SHARIF on 09-Sep-17.
 */
public interface ItemClickListener {
    void onClick(View view, int position, boolean isLongClick);
}
