package com.example.sharif.foodorderapp.Activity;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sharif.foodorderapp.Model.User;
import com.example.sharif.foodorderapp.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class SignUpActivity extends AppCompatActivity {


    EditText userNameET;
    EditText phoneNumberET;
    EditText passwordET;
    Button submitBTN;

    String phoneNumber;
    String password;
    String userName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        userNameET = (EditText) findViewById(R.id.userNameId);
        phoneNumberET = (EditText) findViewById(R.id.phoneNumberId);
        passwordET = (EditText) findViewById(R.id.passwordId);
        submitBTN = (Button) findViewById(R.id.submitId);

        //Init frebase Database
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        final DatabaseReference table_user = firebaseDatabase.getReference("user");

        submitBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userName = userNameET.getText().toString().trim();
                phoneNumber = phoneNumberET.getText().toString().trim();
                password = passwordET.getText().toString().trim();


                if(!TextUtils.isEmpty(userName) || !TextUtils.isEmpty(phoneNumber) || !TextUtils.isEmpty(password)){
                    final ProgressDialog progressDialog = new ProgressDialog(SignUpActivity.this);
                    progressDialog.setMessage("Please Waiting......");
                    progressDialog.show();

                    table_user.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            //Check if already user phone.....

                            if(dataSnapshot.child(phoneNumber).exists()){

                                progressDialog.dismiss();
                                Toast.makeText(SignUpActivity.this, "Phone number already registered", Toast.LENGTH_SHORT).show();

                            }else {

                                progressDialog.dismiss();
                                User user = new User(userName,password);
                                table_user.child(phoneNumber).setValue(user);
                                Toast.makeText(SignUpActivity.this, "Sign Up Successfully", Toast.LENGTH_SHORT).show();
                                finish();
                            }

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
                else{
                    Toast.makeText(SignUpActivity.this, "Fields does not be empty", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


}
